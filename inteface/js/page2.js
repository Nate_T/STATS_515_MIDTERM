var chart;

$( document ).ready(function() {

 $.ajax({
  url:"http://173.79.134.233:72/page2Main/2016",
  type:"GET",
  contentType:"application/json; charset=utf-8",
  dataType:"json",
  success: function(data){
       head=[];
      line1=[];
      line2=[];
    for (i = 0; i < data.length; i++) {
        head.push(data[i]._id.country);
        line1.push(data[i].totalF);
        line2.push(data[i].count);
      }
    chart = chartbottom (head, line2, line1);
    }
});  

});







function chartbottom (head, fatal, counts){

      chart = Highcharts.chart('mainchart', {
        chart: {
            type: 'column',
            spacingBottom:25
        },
        title: {
            text: 'Fatalaties and Incidents Per Year'
        },
        color: 
                ['#2DC4B2',
                '#AA5E79'] 
        ,
        xAxis: {
            categories: head,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Fatalities and Conflits'
            }
        },
        credits:{
            enabled:false
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{color:'#2DC4B2',
            name:'Conflicts',
            data: fatal,


        },
         {
            color:'#AA5E79',
            name: 'Fatalities',
            data: counts

        }]
    });
return chart;

}
