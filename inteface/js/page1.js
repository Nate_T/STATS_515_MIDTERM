var date = 2017;
var events = "Riots or Protests";
var chart;
$( document ).ready(function() {
   $.ajax({
  url:"http://173.79.134.233:72/mapp1/2017/Riots or Protests",
  type:"GET",
  contentType:"application/json; charset=utf-8",
  dataType:"json",
  success: function(data){
     sendall(data)
    }
});  


 $.ajax({
  url:"http://173.79.134.233:72/chartp1/2017",
  type:"GET",
  contentType:"application/json; charset=utf-8",
  dataType:"json",
  success: function(data){
      //console.log(data);
      data.splice(0, 3);
      head=[];
      line1=[];
      line2=[];
    for (i = 0; i < data.length; i++) {
        head.push(data[i]._id.year);
        line1.push(data[i].total);
        line2.push(data[i].count);
      }
     chartbottom (head, line2, line1);
    }
});  


 $.ajax({
  url:"http://173.79.134.233:72/chartt2/2017/",
  type:"GET",
  contentType:"application/json; charset=utf-8",
  dataType:"json",
  success: function(data){
     console.log(data);
      head=[];
      line1=[];
      line2=[];
      for (i = 0; i < data.length; i++) {
        head.push(data[i]._id.event);
        line1.push(data[i].total);
        line2.push(data[i].count);
      }
     chart = chartbottom2 (head, line2, line1);
    }
});  



});


  $('.dropdown1').dropdown({
      stopPropagation: false,
      constrainWidth: false
    }
  );
  $('.dropdown2').dropdown({
      stopPropagation: false,
      constrainWidth: false
    }
  );


var map = new mapboxgl.Map({
    container: 'map', // container id
    maxBounds: [ [-180, -85], [180, 85] ],
    style: {
        "version": 8,
        "sources": {
            "simple-tiles": {
                "type": "raster",
                // point to our third-party tiles. Note that some examples
                // show a "url" property. This only applies to tilesets with
                // corresponding TileJSON (such as mapbox tiles). 
                "tiles": [
                    "http://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    "http://b.tile.openstreetmap.org/{z}/{x}/{y}.png"
                ],
                "tileSize": 256
            }
        },
        "layers": [{
            "id": "simple-tiles",
            "type": "raster",
            "source": "simple-tiles",
            "minzoom": 0,
            "maxzoom": 22
        }]
    },
    center: [0, 0], // starting position
    zoom: 3 // starting zoom
});


function sendall(da){
   map.addLayer({
        "id": "points",
        "type": "circle",
        "source": {
            "type": "geojson",
            "data":  da
        },
         'paint': {
            // make circles larger as the user zooms from z12 to z22
            'circle-radius': {
        property: 'FATALITIES',
        stops: [
          [0, 3],
          [5, 15],
          [16, 20]
        ]
      },
          'circle-color': {
        property: 'FATALITIES',
        stops: [
          [0, '#2DC4B2'],
          [5, '#3BB3C3'],
          [10, '#669EC4'],
          [15, '#8B88B6'],
          [20, '#A2719B'],
          [30, '#AA5E79']
        ]
      },
      'circle-opacity': 0.8
        }
    });

}




map.on('zoom', function() {
});



$("#updatebtn").click(function(){
console.log(date);
console.log(events);
try{
 map.removeLayer("points");
 map.removeSource ("points");
}
catch(err){


}
/*var query = {};
var field = 'data.XYZ';
var operator = {};
operator['$lt'] = 15;
query[field] = operator;
db.event.find(query);*/
$.ajax({
  url:"http://173.79.134.233:72/mapp1/"+date+"/"+String(events),
  type:"GET",
  contentType:"application/json; charset=utf-8",
  dataType:"json",
  success: function(data){
      
     sendall(data)
       
   
  }
});


 $.ajax({
  url:"http://173.79.134.233:72/chartt2/"+date,
  type:"GET",
  contentType:"application/json; charset=utf-8",
  dataType:"json",
  success: function(data){
     console.log(data);
      head=[];
      line1=[];
      line2=[];
      for (i = 0; i < data.length; i++) {
        head.push(data[i]._id.event);
        line1.push(data[i].total);
        line2.push(data[i].count);
      }
      console.log(head);
     console.log(line1);
     console.log(line2);

  chart.series[0].update(
    {
        name: "Conflicts",
        data:line2
    });

     chart.series[1].update(
    {
        name: "Fatalities",
        data:line1
    });

    console.log(chart.xAxis[0]);
chart.xAxis[0].update({
    categories:head

});
chart.title.update({
    text: date+' Fatalaties and Incidents Totals'

});

    chart.redraw();
    }
});  


   
});


$(".year_click").click(function(){
    date = $(this).text();
    $( "#yearpick" ).html( date );
    console.log(date);
});

$(".actor_click").click(function(){
    events =$(this).text();
   $( "#type_pick" ).html( events );
console.log(events);
});

$("#intro_btn").click(function(){
   
    console.log("we made it");
     TweenMax.to("#intro", 1, {autoAlpha: 0});


});


function chartbottom (head, fatal, counts){





       Highcharts.chart('yearcontain', {
        chart: {
            type: 'column',
            spacingBottom:0
        },
        title: {
            text: '2010-2017 Fatalaties and Incidents Totals'
        },
        color: 
                ['#2DC4B2',
                '#AA5E79'] 
        ,
        xAxis: {
            categories: head,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ' Fatalities and Conflits'
            }
        },
        credits:{
            enabled:false
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{color:'#2DC4B2',
            name:'Conflicts',
            data: fatal,


        },
         {
            color:'#AA5E79',
            name: 'Fatalities',
            data: counts

        }]
    });


}


function chartbottom2 (head, fatal, counts){





      chart = Highcharts.chart('onecontain', {
        chart: {
            type: 'column',
            spacingBottom:0
        },
        title: {
            text: 'Fatalaties and Incidents Per Year'
        },
        color: 
                ['#2DC4B2',
                '#AA5E79'] 
        ,
        xAxis: {
            categories: head,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Fatalities and Conflits'
            }
        },
        credits:{
            enabled:false
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{color:'#2DC4B2',
            name:'Conflicts',
            data: fatal,


        },
         {
            color:'#AA5E79',
            name: 'Fatalities',
            data: counts

        }]
    });
return chart;

}