'use strict';

const spawn = require( 'child_process' ).spawn;
var fs = require("fs");
var MongoClient = require('mongodb').MongoClient;
 

//mapshaper -i provinces.shp -simplify dp 20% -o format=geojson out.json

function shpcreate(shp, outjson) {
var ls = spawn( '../node_modules/mapshaper/bin/mapshaper', [ '-i', shp, '-o', 'format=geojson ',  outjson   ] );

ls.stdout.on( 'data', data => {
    console.log( data );
});

ls.stderr.on( 'data', data => {
    console.log( `stderr: ${data}` );
});

ls.on( 'close', code => {
    
    console.log( `Finished creating json exited with code ${code}` );
    var content = fs.readFileSync(outjson);
    var obj = JSON.stringify(content);

    var res = obj.replace('{"type":"FeatureCollection","features":', '');
    res = res.slice(0, -1);
    fs.writeFile(outjson, res, function(err){

        console.log("The file was saved!");



    });
});
};

shpcreate('data/ne_10m_admin_0_countries.shp',"out.json");
