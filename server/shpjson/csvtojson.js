var csv2geojson = require('csv2geojson');
var fs = require('fs');

var latfield = 'LATITUDE';
var longfield = 'LONGITUDE';
var csvfilename = 'two.csv';
var outfilename = 'out4.json';

//outfilename = process.argv[3];
//csvfilename = process.argv[2];

fs.readFile(csvfilename, 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    csv2geojson.csv2geojson(data, {
        latfield: latfield,
        lonfield: longfield,
        delimiter: ','
    }, function (err, data) {
        var obj = JSON.stringify(data);
        var res = obj.replace('{"type":"FeatureCollection","features":', '');
        res = res.slice(0, -1);
        fs.writeFile(outfilename, res, function (err) {
            console.log("The file was saved!");
        });
    });
});

//mongoimport --db dbname -c collectionname --file "output.geojson" --jsonArray