var express = require('express');
var app = express();
var morgan = require('morgan');
var fs = require("fs");
var MongoClient = require('mongodb').MongoClient;
var database = require('./config/database.js')
var url = database.database;


function addgeojson(data){

data = '{"type":"FeatureCollection","features":'+data+"}"
return data
}

app.use(morgan('dev'));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

MongoClient.connect(url, function (err, db) {
    var col = db.collection('africa');

   /* app.get('/:id', function (req, res) {

        console.log(req.params.id)
        values = parseInt(req.params.id)




        col.aggregate([{
      $group: {
                _id: {
                    Country: "$properties.COUNTRY", Eventtype: "$properties.EVENT_TYPE", year: "$properties.YEAR",
                    total: { $sum: "$properties.FATALITIES" }
                }
    }}
      ], function(err, result) {
       res.end(addgeojson(JSON.stringify(result)));
    });




    })*/


    app.get('/mapp1/:id/:id2', function (req, res) {

        console.log(req.params.id)
        values = parseInt(req.params.id);
        values2 = req.params.id2;

        if(values2 == "ALL"){
            col.find({ "properties.YEAR": values },
                {
                    _id: 0, 'properties.GWNO': 0, 'properties.NOTES': 0, 'properties.SOURCE': 0,
                    'properties.ADMIN1': 0, 'properties.ADMIN2': 0, 'properties.ADMIN3': 0,
                    'properties.INTER2': 0, 'properties.INTER1': 0, 'properties.EVENT_ID_CNTY': 0, 'properties.EVENT_ID_NO_CNTY': 0,
                    'properties.INTERACTION': 0, 'properties.ALLY_ACTOR_1': 0, 'properties.ALLY_ACTOR_2': 0


                }
            ).toArray(function (err, items) {
                res.end(addgeojson(JSON.stringify(items)));
                console.log(items)
            })

            
        }
        else{
        
            col.find({
                $and: [
                    { "properties.YEAR": values }, { "properties.EVENT_TYPE": values2 }
                ]
            },
                {
                    _id: 0, 'properties.GWNO': 0, 'properties.NOTES': 0, 'properties.SOURCE': 0,
                    'properties.ADMIN1': 0, 'properties.ADMIN2': 0, 'properties.ADMIN3': 0,
                    'properties.INTER2': 0, 'properties.INTER1': 0, 'properties.EVENT_ID_CNTY': 0, 'properties.EVENT_ID_NO_CNTY': 0,
                    'properties.INTERACTION': 0, 'properties.ALLY_ACTOR_1': 0, 'properties.ALLY_ACTOR_2': 0
                }
            ).toArray(function (err, items) {
                res.end(addgeojson(JSON.stringify(items)));
                console.log(items)
            })
    }

    })

 app.get('/chartp1/:id', function (req, res) {
        console.log(req.params.id);
        values = parseInt(req.params.id);

        col.aggregate(
            [
                {
                    $group:
                    {
                        _id: {"year": "$properties.YEAR"},
                        total : {$sum: "$properties.FATALITIES"},
                        count: {$sum : 1}
                    }
                },
                {
                    $sort: {"_id.year":1}
                }
            ]

        ).toArray(function (err, items) {
            
                
                res.end(JSON.stringify(items));
                console.log(items)
            })

 });


 app.get('/chartt2/:id', function (req, res) {
        console.log(req.params.id);
        values = parseInt(req.params.id);
col.aggregate(
   [
   {'$match': {"properties.YEAR": values}},
   {
     '$group': {
       _id : {"event": "$properties.EVENT_TYPE"},
       total: {$sum: "$properties.FATALITIES"},
       count:{$sum: 1}
     }
   }
   
]).toArray(function (err, items) {
            
                
                res.end(JSON.stringify(items));
                console.log(items)
 })}

);


app.get('/page2Main/:id', function (req, res) {
        console.log(req.params.id);
        values = parseInt(req.params.id);
col.aggregate(
   [
   {'$match': {"properties.YEAR": values}},
   {
     '$group': {
       _id : {"country": "$properties.COUNTRY"},
       totalF: {$sum: "$properties.FATALITIES"},
       count:{$sum: 1}
     }
   }
   
   ]
).toArray(function (err, items) {
            
                
                res.end(JSON.stringify(items));
                console.log(items)
 })}

);



app.get('/maps/:id', function (req, res) {
        console.log(req.params.id);
        values = parseInt(req.params.id);


col.find({"properties.ACTOR1": "LRA: Lords Resistance Army" }).toArray(function (err, items) {
            
                
                res.end(addgeojson(JSON.stringify(items)));
                console.log(items)
 })}

);


















    /*app.get('/:test1/:test', function (req, res) {
        
        console.log(req.params.id)
        joe = parseInt(req.params.id)
        col.find({"properties.GWNO":615}).toArray(function (err, items) {
            res.end(JSON.stringify(items));
            //console.log(items)
        })

    })*/

   /* app.get('/:ja/:great', function (req, res) {

        var mapper = function () {
            emit(this.properties.YEAR, this.properties.YEAR);
        };

        var reducer = function (YEAR, count) {
            return count[0];
        };

        console.log("no")
        col.mapReduce(
            mapper,
            reducer,
            {
                out: "example1_results2"
            }, function (err, collection) {
                console.log("err")
                collection.findOne({}, function (err, result) {
                    console.log(result)
                })
            });



        console.log(req.params.great)
        joe = parseInt(req.params.great)
        col.find({ "properties.FATALITIES": { "$gt": joe } }).toArray(function (err, items) {
            res.end(JSON.stringify(items));
            //console.log(items)
        })
    })*/

});


var server = app.listen(72, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
});

